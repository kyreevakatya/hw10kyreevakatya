<?php

require_once '../vendor/autoload.php';
require_once '../config/eloquent.php';

//$data = [
//    ['title' => 'Category 1', 'slug' => 'slug 1', 'created_at' => date('Y-m-d H:m:s')],
//    ['title' => 'Category 2', 'slug' => 'slug 2', 'created_at' => date('Y-m-d H:m:s')],
//    ['title' => 'Category 3', 'slug' => 'slug 3', 'created_at' => date('Y-m-d H:m:s')],
//    ['title' => 'Category 4', 'slug' => 'slug 4', 'created_at' => date('Y-m-d H:m:s')],
//    ['title' => 'Category 5', 'slug' => 'slug 5', 'created_at' => date('Y-m-d H:m:s')],
//];

//$category = \Hillel\Models\Category::insert($data);

//$category = \Hillel\Models\Category::inRandomOrder()->limit(1)->update([
//    'title' => 'New category', 'slug' => 'new slug'
//]);

//$category = \Hillel\Models\Category::inRandomOrder()->limit(1)->delete();

//$dataPost = [
//    ['title' => 'Post 1', 'slug' => 'slug 1', 'body' => 'body post 1', 'category_id' => 38, 'created_at' => date('Y-m-d H:m:s')],
//    ['title' => 'Post 2', 'slug' => 'slug 2', 'body' => 'body post 2', 'category_id' => 38, 'created_at' => date('Y-m-d H:m:s')],
//    ['title' => 'Post 3', 'slug' => 'slug 3', 'body' => 'body post 3', 'category_id' => 38, 'created_at' => date('Y-m-d H:m:s')],
//    ['title' => 'Post 4', 'slug' => 'slug 4', 'body' => 'body post 4', 'category_id' => 39, 'created_at' => date('Y-m-d H:m:s')],
//    ['title' => 'Post 5', 'slug' => 'slug 5', 'body' => 'body post 5', 'category_id' => 39, 'created_at' => date('Y-m-d H:m:s')],
//    ['title' => 'Post 6', 'slug' => 'slug 6', 'body' => 'body post 6', 'category_id' => 39, 'created_at' => date('Y-m-d H:m:s')],
//    ['title' => 'Post 7', 'slug' => 'slug 7', 'body' => 'body post 7', 'category_id' => 40, 'created_at' => date('Y-m-d H:m:s')],
//    ['title' => 'Post 8', 'slug' => 'slug 8', 'body' => 'body post 8', 'category_id' => 40, 'created_at' => date('Y-m-d H:m:s')],
//    ['title' => 'Post 9', 'slug' => 'slug 9', 'body' => 'body post 9', 'category_id' => 40, 'created_at' => date('Y-m-d H:m:s')],
//    ['title' => 'Post 10', 'slug' => 'slug 10', 'body' => 'body post 10', 'category_id' => 42, 'created_at' => date('Y-m-d H:m:s')]
//];

//$post = \Hillel\Models\Post::insert($dataPost);

//$post = \Hillel\Models\Post::inRandomOrder()->limit(1)->update([
//    'title' => 'New post', 'slug' => 'New slug', 'body' => 'New body', 'category_id' => 39
//]);

//$post = \Hillel\Models\Post::inRandomOrder()->limit(1)->delete();

//$dataTag = [
//    ['title' => 'Tag 1', 'slug' => 'slug 1', 'created_at' => date('Y-m-d H:m:s')],
//    ['title' => 'Tag 2', 'slug' => 'slug 2', 'created_at' => date('Y-m-d H:m:s')],
//    ['title' => 'Tag 3', 'slug' => 'slug 3', 'created_at' => date('Y-m-d H:m:s')],
//    ['title' => 'Tag 4', 'slug' => 'slug 4', 'created_at' => date('Y-m-d H:m:s')],
//    ['title' => 'Tag 5', 'slug' => 'slug 5', 'created_at' => date('Y-m-d H:m:s')],
//    ['title' => 'Tag 6', 'slug' => 'slug 6', 'created_at' => date('Y-m-d H:m:s')],
//    ['title' => 'Tag 7', 'slug' => 'slug 7', 'created_at' => date('Y-m-d H:m:s')],
//    ['title' => 'Tag 8', 'slug' => 'slug 8', 'created_at' => date('Y-m-d H:m:s')],
//    ['title' => 'Tag 9', 'slug' => 'slug 9', 'created_at' => date('Y-m-d H:m:s')],
//    ['title' => 'Tag 10', 'slug' => 'slug 10', 'created_at' => date('Y-m-d H:m:s')],
//];

//$tag = \Hillel\Models\Tag::insert($dataTag);

//$tags = \Hillel\Models\Tag::all()->pluck('id');
//$collection = collect($tags);
//
//$posts = \Hillel\Models\Post::all();
//foreach ($posts as $post) {
//    $post->tags()->sync($collection->random(3)->all());
//}
